﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using ZombisRimworld.UI;

namespace ZombisRimworld
{
    public class Tree : TileObject
    {
        public Tree(Texture2D texture, Tile tile) : base(texture, tile)
        {
            drop = new Log(Game1.textures["log"], tile.Position);
        }

        public override void Update(Map map)
        {
            if(tile.Hovered && MouseEvents.SelectTree && MouseEvents.LeftClick)
            {
                if (map.treesToChop.Contains(this))
                {
                    map.treesToChop.Remove(this);
                }
                else map.treesToChop.Add(this);
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            if (tile.Hovered && MouseEvents.SelectTree)
            {
                spriteBatch.Draw(Game1.textures["hoverTile"], tile.Position, Color.White);
            }
        }
    }
}
